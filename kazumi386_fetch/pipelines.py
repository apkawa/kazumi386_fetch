# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
from scrapy.contrib.pipeline.images import ImagesPipeline
import os

class Kazumi386FetchImagePipeline(ImagesPipeline):
    pass
    def process_item(self, item, spider):
        self._item = item
        return super(Kazumi386FetchImagePipeline, self).process_item(item, spider)

    def image_key(self, url):
        image = os.path.split(url)[1]
        maid_type = self._item['maid_type']
        return '%s/%s' % (maid_type, image)

    def thumb_key(self, url, thumb_id):
        image = os.path.split(url)[0]
        maid_type = self._item['maid_type']
        return '%s/thumb/%s/%s' % (maid_type, thumb_id, image)

