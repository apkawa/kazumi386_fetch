import socket
import random

from scrapy.contrib.downloadermiddleware.httpproxy import HttpProxyMiddleware
from scrapy import conf
from scrapy.exceptions import NotConfigured
from scrapy import log


COUNTER_ATTR = '__tor_swith_attr'
DEFAULT_COUNT = 10

class TorSwitchIPMiddleware(object):
    def get_count(self, spider):
        count = getattr(spider, COUNTER_ATTR, None)
        if count is None:
            count = DEFAULT_COUNT

        self.set_count(spider, count - 1)
        return count

    def set_count(self, spider, count=DEFAULT_COUNT):
        setattr(spider, COUNTER_ATTR, count)

    def tor_switch_ip(self):
        TOR_IP = '127.0.0.1'
        TOR_PORT = 9051

        AUTH_MSG = "authenticate \n"
        CHANGE_MSG = "signal newnym \n"
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((TOR_IP, TOR_PORT))
        sock.send(AUTH_MSG)
        sock.recv(512)
        sock.send(CHANGE_MSG)
        sock.recv(512)
        sock.close()

    def process_request(self, request, spider):

        count = self.get_count(spider)

        if count <= 0:
            log.msg("Tor switched ip", level=log.DEBUG)
            self.tor_switch_ip()


class CustomHttpProxyMiddleware(HttpProxyMiddleware):
    def __init__(self):
        super(CustomHttpProxyMiddleware, self).__init__()
        settings = conf.settings
        self.proxies = {}
        for schema in ['http', 'https']:
            url = settings.get(schema.upper() + '_PROXY')
            if url:
                self.proxies[schema] = self._get_proxy(url, schema)

        if not self.proxies:
            raise NotConfigured


class RandomUserAgentMiddleware(object):

    def get_user_agent(self):
        return random.choice([
            'Mozilla/5.0 (X11; Linux i686) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11'
        ])

    def process_request(self, request, spider):
        user_agent  = self.get_user_agent()
        if user_agent:
            request.headers.setdefault('User-Agent', user_agent)