# Scrapy settings for kazumi386_fetch project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'kazumi386_fetch'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['kazumi386_fetch.spiders']
NEWSPIDER_MODULE = 'kazumi386_fetch.spiders'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.httpcache.HttpCacheMiddleware': 100,
    'scrapy.contrib.downloadermiddleware.httpcache.DepthMiddleware': None,

    'kazumi386_fetch.middleware.CustomHttpProxyMiddleware': 300,
    'kazumi386_fetch.middleware.RandomUserAgentMiddleware': 400,
    'kazumi386_fetch.middleware.TorSwitchIPMiddleware': 1000,
    }


HTTPCACHE_ENABLED = True
HTTPCACHE_EXPIRATION_SECS = 0
HTTPCACHE_DIR = '/media/hd1000/maid/.cache/'

HTTP_PROXY = 'http://127.0.0.1:8118'

ITEM_PIPELINES = [
    'kazumi386_fetch.pipelines.Kazumi386FetchImagePipeline',
]

IMAGES_STORE = '/media/hd1000/maid/'