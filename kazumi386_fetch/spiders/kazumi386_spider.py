import os
import re
from scrapy.spider import BaseSpider
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector

from kazumi386_fetch.items import Kazumi386FetchItem


class MaApiSpider(BaseSpider):
    http_user = 'kazumi'
    http_pass = 'kazumi'
    name = "kazumi"

    allowed_domains = ["kazumi386.org"]

    start_urls = [
        "http://kazumi386.org/~maid/allmenu.html",
        ]

    def start_requests(self):
        return [Request("http://kazumi386.org/~maid/allmenu.html", callback=self.parse_allmenu)]

    def _get_type_by_link(self, link):
        return '_'.join(re.findall(r'([\w\d]+)', link))

    def _get_callback(self, callback, *args, **kwargs):

        def wrap(request):
            return callback(request, *args, **kwargs)

        return wrap

    def parse_item(self, response, link):
        hxs = HtmlXPathSelector(response)
        abs_link = 'http://kazumi386.org/~maid/' + link
        image_url = abs_link + hxs.select("//meta[@http-equiv='refresh']/@content").re(r'(/src/\d+\.\w+)$')[0]
        return Kazumi386FetchItem(
            maid_type=self._get_type_by_link(link),
            image_urls=[image_url]
        )

    def parse_view_all(self,response, link, page=1):
        hxs = HtmlXPathSelector(response)
        abs_link = 'http://kazumi386.org/~maid/' + link

#        from scrapy.shell import inspect_response
#        inspect_response(response, self)
        for url_item in hxs.select('//td/a/@href').extract():
            yield Request(abs_link + url_item, callback=self._get_callback(self.parse_item, link),
                        meta={"dont_redirect": True})

        max_page = max(map(int, hxs.select('//a[contains(@href, "viewa.php?pg=")]/@href').re(r'viewa.php\?pg=(\d+)')))
        next_page = page + 1
        if not (next_page > max_page):
            yield Request(abs_link + 'viewa.php?pg=%s' % int(next_page),
                            callback=self._get_callback(self.parse_view_all, link, page=next_page))

    def parse_menu(self,response, link):
        hxs = HtmlXPathSelector(response)
        abs_link = 'http://kazumi386.org/~maid/' + link
        for piece in hxs.select('//a[contains(@href, "viewa.php")]/@href').extract():
            hogo = os.path.split(piece)[0] + '/'
            print abs_link + piece
            yield Request(abs_link + piece, callback=self._get_callback(self.parse_view_all, link + hogo))


    def parse_allmenu(self, response):
        hxs = HtmlXPathSelector(response)
        links = hxs.select("//a[starts-with(@href,'./_')]/@href").extract()
        for link in links:
            abs_link = 'http://kazumi386.org/~maid/' + link
            yield Request(abs_link + 'menu.html', callback=self._get_callback(self.parse_menu, link))


